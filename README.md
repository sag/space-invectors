# Space (in)vectors

A simple game alike Space invaders in pure JavaScript with SVG icons.

## Usage

Since the game is an HTML document, you can download it and play it completely offline. However, you can try the [live demo here](https://sagxd.itch.io/space-invectors).

## ToD0
- [x] Pixelart Font
- [x] Change Keybinds
- [ ] Remake All SVGs in Pixelart Style
- [ ] New Boosts and Mechanics

## Screenshots

![image1](./screenshots/image1.png)

![image2](./screenshots/image2.png)

![image3](./screenshots/image3.png)

![image4](./screenshots/image4.png)

![image5](./screenshots/image5.png)

## License

This game and its SVG images are released under the terms of the AGPLv3 license. See LICENSE for more details.

It uses the forkawesome font (see forkawesome/LICENSES.txt) for more information.
